package password;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckPasswordLength() {
		assertTrue("Password is invalid", 
				PasswordValidator.checkPasswordLength("thisisvalidapassword")); 
		// checkPasswordLength returns boolean
	}
	
	@Test
	public void testCheckPasswordLengthException() {
		assertFalse("Invalid password false", 
				PasswordValidator.checkPasswordLength("fail") );
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		assertTrue("Password is invalid", 
				PasswordValidator.checkPasswordLength("12345678")); 
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		assertFalse("Invalid password false", 
				PasswordValidator.checkPasswordLength("1234567") );
	}
	
	@Test
	public void testCheckPasswordDigits() {
		assertTrue("Password is invalid",
				PasswordValidator.checkPasswordDigits("thisisvalidpassword1234567890"));
	}
	
	@Test
	public void testCheckPasswordDigitsExcepation() {
		assertFalse("Password is valid",
				PasswordValidator.checkPasswordDigits("thisisvalidpassword"));
	}
	
	@Test
	public void testCheckPasswordDigitsBoundaryIn() {
		assertTrue("Password is invalid",
				PasswordValidator.checkPasswordDigits("thisisvalidpassword12"));
	}
	
	@Test
	public void testCheckPasswordDigitsBoundaryOut() {
		assertFalse("Password is valid",
				PasswordValidator.checkPasswordDigits("thisisvalidpassword1"));
	}

}
