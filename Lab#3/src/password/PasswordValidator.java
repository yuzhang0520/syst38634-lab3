package password;

public class PasswordValidator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static boolean checkPasswordLength(String password) {
		return password.length() >= 8;
//		if (password.length() >= 8 ) {
//			return true;
//		}
//		else {
//			return false;
//		}
	}

	// Before
//	public static boolean checkPasswordDigits(String password) {
//		int totalDigits = 0;
//		for(int i = 0; i < password.length(); i++) {
//			if (Character.isDigit(password.charAt(i)));
//			totalDigits++;
//		}
//		System.out.println(totalDigits);
//		return totalDigits > 2;
//	}
//	
	// After
	public static boolean checkPasswordDigits(String password) {
		int totalDigits = 0;
		for(int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i))){
				totalDigits++;
			}
		}
		System.out.println(totalDigits);
		return totalDigits >= 2;
	}
}
